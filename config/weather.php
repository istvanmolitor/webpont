<?php

return [
    'api_url' => 'https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s',
    'api_key' => env('WEATHER_API_KEY', '')
];
