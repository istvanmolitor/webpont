<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('weather_informations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('lat', 8,4);
            $table->float('lon', 8, 4);
            $table->integer('temperature')->nullable();
            $table->integer('pressure')->nullable();
            $table->integer('humidity')->nullable();
            $table->integer('temp_min')->nullable();
            $table->integer('temp_max')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('weather_informations');
    }
};
