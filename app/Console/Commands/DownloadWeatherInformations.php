<?php

namespace App\Console\Commands;

use App\Repositories\CityRepository;
use App\Repositories\WeatherInformationRepository;
use Illuminate\Console\Command;

class DownloadWeatherInformations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $weatherInformationRepository = new WeatherInformationRepository();
        $cities = (new CityRepository())->getAll();
        foreach ($cities as $city) {
            $weatherInformationRepository->downloadByCity($city);
        }
    }
}
