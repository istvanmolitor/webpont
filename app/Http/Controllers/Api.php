<?php

namespace App\Http\Controllers;

use App\Http\Resources\WeatherInformationResource;
use App\Repositories\WeatherInformationRepository;
use Illuminate\Http\Request;

class Api extends Controller
{
    public function index(WeatherInformationRepository $repository)
    {
        return WeatherInformationResource::collection($repository->getAll());
    }

    public function search(WeatherInformationRepository $repository, Request $request)
    {
        $city = $request->get('city', '');
        return WeatherInformationResource::collection($repository->search($city));
    }
}
