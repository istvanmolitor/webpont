<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeatherInformation extends Model
{
    protected $table = 'weather_informations';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'lat',
        'lon',
        'temperature',
        'pressure',
        'humidity',
        'temp_min',
        'temp_max'
    ];

    public $timestamps = true;
}
