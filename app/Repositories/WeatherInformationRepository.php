<?php

namespace App\Repositories;

use App\Models\City;
use App\Models\WeatherInformation;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;

class WeatherInformationRepository extends Repository
{

    protected function makeModel(): WeatherInformation
    {
        return new WeatherInformation();
    }

    public function download(float $lat, float $lng): ?WeatherInformation
    {
        $url = sprintf(config('weather.api_url'), $lat, $lng, config('weather.api_key'));

        $client = new Client();
        $json = $client->get($url)->getBody();
        $data = json_decode($json->getContents());

        return $this->model->create([
            'name' => $data->name,
            'lat' => (float)$data->coord->lat,
            'lon' => (float)$data->coord->lon,
            'temperature' => (int)$data->main->temp,
            'pressure' => (int)$data->main->pressure,
            'humidity' => (int)$data->main->humidity,
            'temp_min' => (int)$data->main->temp_min,
            'temp_max' => (int)$data->main->temp_max,
        ]);
    }

    public function downloadByCity(City $city): ?WeatherInformation
    {
        return $this->download($city->lat, $city->lon);
    }

    public function getAll(): Collection
    {
        return $this->model->orderBy('created_at')->get();
    }

    public function search(mixed $city)
    {
        $time = Carbon::now()->addDays(-1);

        return $this->model->where('name', 'LIKE', "%{$city}%")
            ->orderBy('created_at')
            ->where('created_at', '>=', $time)
            ->get();
    }
}
