<?php

namespace App\Repositories;

use App\Models\City;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class CityRepository extends Repository
{
    protected function makeModel(): City
    {
        return new City();
    }

    public function getAll(): Collection
    {
        return $this->model->orderBy('name')->get();
    }
}
